**Problem definition:**

*Write a method or function in the major programming language of your choice that returns the longest word in a sentence and its length. For example, “The cow jumped over the moon.” should return “jumped” and 6.
Write unit tests, reworking code as needed
Add a method that returns the shortest word and length with unit tests
Create a README documenting any assumptions you made and including instructions on how to build and execute your tests.
Share your code using GitHub or similar.*

**Assumption: **
* When string is blank, message should be given "No sentence entered!!!"
* Sentence where multiple words are of same length, word which has alphabetical precedence will be returned.


    
To run tests: **mvn test**