package com.troweprice;

import java.util.Scanner;
import static java.lang.System.exit;

public class Main {

    public static void main(String[] args) {
        System.out.print("Enter a sentence to find its longest and smallest word: ");
        Scanner scanner = new Scanner(System. in);
        String input = scanner. nextLine();
        Sentence sentence = new Sentence(input);
        DomainSentence smallWord = sentence.getSmallestWordAndLength();
        DomainSentence longWord = sentence.getLongestWordAndLength();
        if(smallWord==null | longWord==null) {
            System.out.println("No sentence entered!!!");
            exit(0);
        }
        System.out.println("Smallest word: " + smallWord.getWord() + " (" + smallWord.getLength() + " letters)");
        System.out.println("Longest word: " + longWord.getWord() + " (" + longWord.getLength() + " letters)");
    }
}
