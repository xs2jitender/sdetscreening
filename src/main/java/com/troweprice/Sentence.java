package com.troweprice;

import java.util.Arrays;
import java.util.Comparator;

public class Sentence {
    private String sentence;

    public Sentence(String sentence) {
        this.sentence = sentence.replaceAll(" +", " ");;
    }

    public DomainSentence getLongestWordAndLength() {
        if (isNull()) return null;
        String[] words = sentence.split(" ");
        Arrays.sort(words, getWordComparator());
        return new DomainSentence(words[words.length-1], words[words.length-1].length());
    }

    public DomainSentence getSmallestWordAndLength() {
        if(isNull()) return null;
        String[] words = sentence.split(" ");
        Arrays.sort(words, getWordComparator());
        return new DomainSentence(words[0], words[0].length());
    }

    private boolean isNull() {
        return "".equals(sentence);
    }

    private Comparator getWordComparator() {
        return new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if( (o1.length()==o2.length())) {
                    return o1.compareTo(o2);
                }
                return o1.length() - o2.length();
            }
        };
    }

}
