package com.troweprice;

import org.junit.Assert;
import org.junit.Test;

public class SentenceTests {
    public static final String NORMAL_SENTENCE="This is a sentence";
    public static final String SAME_LENGTH_WORD_SENTENCE="This is good test sentence for testing my code";


    @Test
    public void shouldFindLongest() {
        Sentence sentence = new Sentence(NORMAL_SENTENCE);
        DomainSentence longestWordAndLength = sentence.getLongestWordAndLength();
        Assert.assertEquals("Checking longest word","sentence",longestWordAndLength.getWord());
        Assert.assertEquals("Checking longest word's length",8,longestWordAndLength.getLength());
    }

    @Test
    public void shouldFindSmallest() {
        Sentence sentence = new Sentence(NORMAL_SENTENCE);
        DomainSentence smallestWordAndLength = sentence.getSmallestWordAndLength();
        Assert.assertEquals("Checking smallest word","a",smallestWordAndLength.getWord());
        Assert.assertEquals("Checking smallest word's length",1,smallestWordAndLength.getLength());
    }

    @Test
    public void shouldFindLongestAndAlphabeticallyFirst() {
        Sentence sentence = new Sentence(SAME_LENGTH_WORD_SENTENCE);
        DomainSentence longestWordAndLength = sentence.getLongestWordAndLength();
        Assert.assertEquals("Checking longest word","sentence",longestWordAndLength.getWord());
        Assert.assertEquals("Checking longest word's length",8,longestWordAndLength.getLength());
    }

    @Test
    public void shouldFindSmallestAndAlphabeticallyLast() {
        Sentence sentence = new Sentence(SAME_LENGTH_WORD_SENTENCE);
        DomainSentence smallestWordAndLength = sentence.getSmallestWordAndLength();
        Assert.assertEquals("Checking smallest word","is",smallestWordAndLength.getWord());
        Assert.assertEquals("Checking smallest word's length",2,smallestWordAndLength.getLength());
    }

    @Test
    public void shouldHandleNull() {
        Sentence sentence = new Sentence("");
        DomainSentence smallestWordAndLength = sentence.getSmallestWordAndLength();
        Assert.assertNull(smallestWordAndLength);
        DomainSentence longestWordAndLength = sentence.getLongestWordAndLength();
        Assert.assertNull(longestWordAndLength);
    }

    @Test
    public void shouldHandleMultipleSpaces() {
        Sentence sentence = new Sentence("This is     a      rubbish sentence");
        DomainSentence longestWordAndLength = sentence.getLongestWordAndLength();
        Assert.assertEquals("Checking longest word","sentence",longestWordAndLength.getWord());
        Assert.assertEquals("Checking longest word's length",8,longestWordAndLength.getLength());
        DomainSentence smallestWordAndLength = sentence.getSmallestWordAndLength();
        Assert.assertEquals("Checking smallest word","a",smallestWordAndLength.getWord());
        Assert.assertEquals("Checking smallest word's length",1,smallestWordAndLength.getLength());
    }

}
